package com.ondrejsynak.scan.acitivityscanner

import android.view.View
import java.lang.reflect.Field

/**
 * @return List of [View] using the reflection to fetch [WindowGlobalManager] and it's views.
 */
internal fun getWindowManagerRootViews(): List<View?> {
    try {
        val wmgClass = Class.forName("android.view.WindowManagerGlobal")
        val wmgInstance = wmgClass.getMethod("getInstance").invoke(null)
        return viewsFromWM(wmgClass, wmgInstance)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return emptyList()
}

private fun viewsFromWM(wmClass: Class<*>, wmInstance: Any): List<View?> {
    val viewsField: Field = wmClass.getDeclaredField("mViews")
    viewsField.isAccessible = true
    val views: Any = viewsField.get(wmInstance)
    if (views is List<*>) {
        return viewsField.get(wmInstance) as ArrayList<View?>
    } else if (views is Array<*>) {
        return views.asList().filterIsInstance<View>()
    }
    return emptyList()
}