package com.ondrejsynak.scan.acitivityscanner

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Canvas
import android.view.ViewGroup
import androidx.core.view.get
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

object ActivityScanner {

    /**
     * @return [Bitmap] Wireframe with view represented as rectangles from given [activity].
     */
    fun renderWireframe(activity: Activity): Bitmap? {
        val rootViews = getWindowManagerRootViews()

        val finalBitmap = prepareBitmap(activity)
        val drawingCanvas = Canvas(finalBitmap)

        rootViews.filterNotNull().forEach {
            it.renderBitmap(drawingCanvas, it.calculateOffsetToScreen())
        }

        return finalBitmap
    }

    private fun prepareBitmap(activity: Activity): Bitmap {
        val rootView = (activity.window.decorView.rootView as ViewGroup)[0]
        return Bitmap.createBitmap(rootView.width, rootView.height, Bitmap.Config.ARGB_8888)
    }
}