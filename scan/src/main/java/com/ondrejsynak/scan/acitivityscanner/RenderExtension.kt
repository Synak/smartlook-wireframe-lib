package com.ondrejsynak.scan.acitivityscanner

import android.graphics.*
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.core.view.children
import androidx.core.view.isVisible
import androidx.core.widget.TintableCompoundButton

/**
 * Renders [View] and all of it's children if there are some.
 *
 * Only goes through visible views.
 */
internal fun View.renderBitmap(canvas: Canvas, offset: Point) {
    if (isVisible) {
        when (this) {
            is ProgressBar -> this.render(canvas, offset)
            is SwitchCompat -> this.render(canvas, offset)
            is RadioButton -> this.render(canvas, offset)
            is TextView -> this.render(canvas, offset)
            is ImageView -> this.render(canvas, offset)
            else -> this.render(canvas, offset)
        }
        if (this is ViewGroup && this.childCount > 0 && alpha > 0) {
            children.forEach { it.renderBitmap(canvas, offset) }
        }
    }
}

private fun View.render(canvas: Canvas, offset: Point) {
    val rect = getGlobalVisibleRect(offset)
    val backgroundColor = getColor(background)

    canvas.drawRect(rect, Paint().apply {
        color = backgroundColor
        style = if (backgroundColor != Color.TRANSPARENT) Paint.Style.FILL else Paint.Style.STROKE
        if (backgroundColor != Color.TRANSPARENT) {
            alpha = (this@render.alpha * 255).toInt()
        }
    })
}

private fun ImageView.render(canvas: Canvas, offset: Point) {
    val globalVisibleRect = getGlobalVisibleRect(offset)
    val imageColor = getColor(drawable)

    canvas.drawRect(globalVisibleRect, Paint().apply {
        color = imageColor
        style = Paint.Style.FILL
    })
}

private fun SwitchCompat.render(canvas: Canvas, offset: Point) {
    val globalVisibleRect = getGlobalVisibleRect(offset)
    canvas.drawRect(globalVisibleRect, Paint().apply {
        color = thumbDrawable.getColor()
        style = Paint.Style.FILL
    })
}

private fun ProgressBar.render(canvas: Canvas, offset: Point) {
    val globalVisibleRect = getGlobalVisibleRect(offset)
    canvas.drawRect(globalVisibleRect, Paint().apply {
        color = Color.WHITE
        style = Paint.Style.STROKE
        strokeWidth = 3f
    })
}

private fun RadioButton.render(canvas: Canvas, offset: Point) {
    val globalVisibleRect = getGlobalVisibleRect(offset)
    val currentColor = if (this is TintableCompoundButton) {
        supportButtonTintList?.defaultColor ?: currentTextColor
    } else {
        currentTextColor
    }
    canvas.drawRect(globalVisibleRect, Paint().apply {
        color = currentColor
        style = Paint.Style.FILL
    })
}