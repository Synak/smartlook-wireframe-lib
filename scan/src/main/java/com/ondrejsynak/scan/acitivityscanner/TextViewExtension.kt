package com.ondrejsynak.scan.acitivityscanner

import android.graphics.*
import android.widget.TextView

private const val DRAWABLE_LEFT = 0
private const val DRAWABLE_TOP = 1
private const val DRAWABLE_RIGHT = 2
private const val DRAWABLE_BOTTOM = 3

/**
 * Renders [TextView] on given [canvas]. Text and compound drawables.
 */
internal fun TextView.render(canvas: Canvas, offset: Point) {
    val globalVisibleRect = getGlobalVisibleRect(offset)

    renderDrawables(globalVisibleRect, canvas)
    renderLines(globalVisibleRect, canvas, Paint().apply {
        color = currentTextColor
        style = Paint.Style.FILL
    })
}

/**
 * Draw all lines of given [TextView]. Each line has it's own rectangle in order better display
 * where the text ends.
 */
private fun TextView.renderLines(globalVisibleRect: Rect, canvas: Canvas, textColorPaint: Paint) {
    (0 until lineCount).forEach {
        val lineRect = Rect()
        getLineBounds(it, lineRect)

        // Find out start and end offset.
        val lineVisibleEnd = (layout?.getLineVisibleEnd(it) ?: 0)
        val lineStart = (layout?.getLineStart(it) ?: 0)

        // Find out start and end.
        val realTextEnd = layout?.getPrimaryHorizontal(lineVisibleEnd) ?: 0f
        val realTextStart = layout?.getPrimaryHorizontal(lineStart) ?: 0f

        canvas.drawRect(
            (globalVisibleRect.left + lineRect.left + realTextStart),
            (globalVisibleRect.top + lineRect.top).toFloat(),
            (globalVisibleRect.left + lineRect.left + realTextEnd),
            (globalVisibleRect.top + lineRect.bottom).toFloat(),
            textColorPaint
        )
    }
}

/**
 * Draw compound drawables of given [TextView]
 */
private fun TextView.renderDrawables(globalVisibleRect: Rect, canvas: Canvas) {
    val centerY = (globalVisibleRect.top + globalVisibleRect.bottom) / 2f
    val centerX = (globalVisibleRect.left + globalVisibleRect.right) / 2f

    compoundDrawables[DRAWABLE_LEFT]?.let {
        val height = it.intrinsicHeight / 2f
        drawCompound(
            canvas = canvas,
            left = (globalVisibleRect.left + paddingStart).toFloat(),
            top = centerY - height,
            right = (globalVisibleRect.left + paddingStart + it.bounds.width()).toFloat(),
            bottom = centerY + height,
            colorPaint = getColor(it)
        )
    }
    compoundDrawables[DRAWABLE_TOP]?.let {
        val width = it.intrinsicWidth / 2f
        drawCompound(
            canvas = canvas,
            left = centerX - width,
            top = (globalVisibleRect.top + paddingTop).toFloat(),
            right = centerX + width,
            bottom = (globalVisibleRect.top + paddingTop + it.bounds.height()).toFloat(),
            colorPaint = getColor(it)
        )
    }
    compoundDrawables[DRAWABLE_RIGHT]?.let {
        val height = it.intrinsicHeight / 2f
        drawCompound(
            canvas = canvas,
            left = (globalVisibleRect.right - it.bounds.width() - paddingEnd).toFloat(),
            top = centerY - height,
            right = (globalVisibleRect.right - paddingEnd).toFloat(),
            bottom = centerY + height,
            colorPaint = getColor(it)
        )
    }
    compoundDrawables[DRAWABLE_BOTTOM]?.let {
        val width = it.intrinsicWidth / 2f
        drawCompound(
            canvas = canvas,
            left = centerX - width,
            top = (globalVisibleRect.bottom - paddingBottom - it.bounds.height()).toFloat(),
            right = centerX + width,
            bottom = (globalVisibleRect.bottom - paddingBottom).toFloat(),
            colorPaint = getColor(it)
        )
    }
}

private fun drawCompound(
    canvas: Canvas,
    left: Float,
    top: Float,
    right: Float,
    bottom: Float,
    colorPaint: Int
) {
    canvas.drawRect(
        left,
        top,
        right,
        bottom,
        Paint().apply {
            color = colorPaint
            style = Paint.Style.FILL
        })
}