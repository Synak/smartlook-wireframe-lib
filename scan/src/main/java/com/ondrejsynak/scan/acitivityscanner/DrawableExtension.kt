package com.ondrejsynak.scan.acitivityscanner

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.*
import android.view.View
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable
import androidx.core.view.drawToBitmap
import androidx.core.widget.TintableImageSourceView
import com.google.android.material.shape.MaterialShapeDrawable

private const val COLOR_UNDEFINED = -124

/**
 * Tries to find Color of the [Drawable]. Each type of drawable has different way how to detect
 * dominant/overall color. If the color cannot be easily determined. Algorithm is used to render
 * view on [Bitmap] and find average color.
 *
 * @return Color of the [Drawable].
 */
@SuppressLint("NewApi")
internal fun Drawable?.getColor(): Int {
    return when (this) {
        is ColorDrawable -> color
        is MaterialShapeDrawable -> fillColor?.defaultColor ?: Color.TRANSPARENT
        is RippleDrawable -> Color.TRANSPARENT
        is ShapeDrawable -> paint.color
        is StateListDrawable -> current.getColor()
        is GradientDrawable -> color?.defaultColor ?: COLOR_UNDEFINED
        is DrawerArrowDrawable -> paint.color
        is VectorDrawable -> COLOR_UNDEFINED
        null -> Color.TRANSPARENT
        else -> COLOR_UNDEFINED
    }
}

@SuppressLint("RestrictedApi")
internal fun View.getColor(drawable: Drawable?): Int {
    drawable ?: return Color.TRANSPARENT

    // First try to resolve color the normal way.
    var finalColor = if (this is TintableImageSourceView) {
        supportImageTintList?.defaultColor ?: drawable.getColor()
    } else {
        drawable.getColor()
    }

    // if the color was not found, try to find average color on the bitmap.
    finalColor = if (finalColor == COLOR_UNDEFINED) {
        try {
            drawToBitmap().getAverageColor()
        } catch (e: Exception) {
            finalColor
        }
    } else finalColor

    return finalColor
}

/**
 * Looks for average color in the [Bitmap].
 * Goes through each pixel and calculates average of each color channel.
 *
 * @return [Int] Color which is average of all pixels.
 */
internal fun Bitmap?.getAverageColor(): Int {
    this ?: return Color.TRANSPARENT

    val width: Int = width
    val height: Int = height

    val pixels = IntArray(width * height)
    getPixels(pixels, 0, width, 0, 0, width, height)

    var r = 0
    var g = 0
    var b = 0
    var count = 0

    // Calculated average of each color channel.
    pixels.indices.forEach {
        val color = pixels[it]
        val a = Color.alpha(color)
        // Ignore transparent colors.
        if (a > 0) {
            r += Color.red(color)
            g += Color.green(color)
            b += Color.blue(color)
            count++
        }
    }

    r /= count
    g /= count
    b /= count

    // Make the color great again! (Make it one number)
    r = r shl 16 and 0x00FF0000
    g = g shl 8 and 0x0000FF00
    b = b and 0x000000FF

    return (-0x1000000 or r or g or b)
}