# Wireframe maker

## See also
- [compiled library](aar/)
- [bitmap conversion examples](examples/)

## About

This library transforms activity to bitmap with function

```kotlin
fun renderWireframe(activity: Activity): Bitmap?
```

## TBD

1. Properly find VectorDrawable's color.
2. Handle all exceptions and lower SDKs.