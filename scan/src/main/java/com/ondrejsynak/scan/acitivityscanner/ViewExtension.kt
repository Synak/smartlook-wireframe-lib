package com.ondrejsynak.scan.acitivityscanner

import android.graphics.Point
import android.graphics.Rect
import android.view.View
import android.view.WindowManager

/**
 * @return [Rect] representing absolute position of the [View] on screen.
 */
internal fun View.getGlobalVisibleRect(offset: Point): Rect {
    val rect = Rect()
    getGlobalVisibleRect(rect)
    rect.offset(offset.x, offset.y)
    return rect
}

/**
 * @return [Point] offset to global screen root.
 */
internal fun View.calculateOffsetToScreen(): Point {
    return (layoutParams as? WindowManager.LayoutParams)?.let {
        Point(it.x, it.y)
    } ?: Point(0, 0)
}
